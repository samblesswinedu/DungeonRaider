 // Fill out your copyright notice in the Description page of Project Settings.


#include "GrabberComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UGrabberComponent::UGrabberComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabberComponent::BeginPlay()
{
	Super::BeginPlay();

	world = GetWorld();
}


// Called every frame
void UGrabberComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector StartPoint = GetComponentLocation();
	FVector EndPoint = GetComponentLocation() + (GetForwardVector() * m_MaxDistance);

	DrawDebugLine
	(
		world,
		StartPoint,
		EndPoint,
		FColor::White
	);

	FVector TargetLocation = GetComponentLocation() + (GetForwardVector() * m_HoldDistance);
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();

	if (PhysicsHandle == nullptr)
	{
		return;
	}

	/*update the position and rotation of physics handler*/
	if (GetPhysicsHandle()->GetGrabbedComponent() !=nullptr)
	{
		GetPhysicsHandle()->SetTargetLocationAndRotation(TargetLocation, GetComponentRotation());
	}
}

UPhysicsHandleComponent* UGrabberComponent::GetPhysicsHandle() const
{
	UPhysicsHandleComponent* PhysicsHandler = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	/*Assertion : whatever is "asserted" must be true, or the program will stop running*/
	checkf(PhysicsHandler != nullptr, TEXT("PhysicsHandleComponent not found"));
	return PhysicsHandler;
}

void UGrabberComponent::Grab()
{
	FVector StartPoint = GetComponentLocation();
	FVector EndPoint = GetComponentLocation() + (GetForwardVector() * m_MaxDistance);

	FCollisionShape Sphere = FCollisionShape::MakeSphere(m_GrabRadius);

	FHitResult outHitResult;

	/*Sphere cast*/
	bool HasHit = world->SweepSingleByChannel
	(
		outHitResult,
		StartPoint,
		EndPoint,
		FQuat::Identity,
		ECC_GameTraceChannel2,
		Sphere
	);

	DrawDebugSphere
	(
		world,
		outHitResult.ImpactPoint,  //get the exact hit point on the object
		10,
		10,
		FColor::Green,
		false,
		5
	);

	if (HasHit)
	{
		UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();

		if (PhysicsHandle == nullptr)
		{
			return;
		}

		UPrimitiveComponent* HitComponent = outHitResult.GetComponent();

		/*Ensure to simulate physics*/
		HitComponent->WakeAllRigidBodies();

		/*To grab the hit object*/
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			HitComponent,
			NAME_None,
			outHitResult.ImpactPoint,
			GetComponentRotation()
		);
	}
}

void UGrabberComponent::Release()
{
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();

	if (PhysicsHandle == nullptr)
	{
		return;
	}

	if (PhysicsHandle->GetGrabbedComponent() != nullptr)
	{
		GetPhysicsHandle()->ReleaseComponent();
	}
}



