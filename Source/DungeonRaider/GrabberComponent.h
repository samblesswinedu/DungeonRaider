// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GrabberComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONRAIDER_API UGrabberComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabberComponent();

	UFUNCTION(BlueprintCallable)
		void Release();
	UFUNCTION(BlueprintCallable)
		void Grab();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UWorld* world;

	UPROPERTY(EditAnywhere)
		float m_MaxDistance = 4000;

	UPROPERTY(EditAnywhere)
		float m_GrabRadius = 100;

	UPROPERTY(EditAnywhere)
		float m_HoldDistance = 200;

private:
	UPhysicsHandleComponent* GetPhysicsHandle() const;
};
